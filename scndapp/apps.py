from django.apps import AppConfig


class ScndappConfig(AppConfig):
    name = 'scndapp'
