from django.contrib import admin
from scndapp.models import Student,Contact,Category,signup,add_product,cart,Order

admin.site.site_header="My_Website | Scnd Project"

class StudentAdmin(admin.ModelAdmin):
    # fields = ["name","email","rno"]       # order chnge or select prticulr field
    list_display = ["name","rno","email","fee","gender","address","is_registered"]
                        # to show in table form
    search_fields = ["name","rno"]          # making a search bar
    list_filter = ["name","gender"]         # make a filter box by which we can search data
    list_editable = ["email"]    # making fields editable in table itself

class ContactAdmin(admin.ModelAdmin):
    fields = ["contact","name","message","subject"]
    list_display = ["id","name","contact","message","subject","added_on"]
    search_fields = ["name"]
    list_filter = ["added_on","name"]
    list_editable = ["name","subject"]
    # here first field is non-editable... So if we want to make first field editable, 
    # we have to change its order by fields(filter)

class CategoryAdmin(admin.ModelAdmin):
    fields = ["cname","description","added_on"]
    list_display = ["id","cname","description","added_on"]

# Register your models here.

admin.site.register(Student,StudentAdmin)
admin.site.register(Contact,ContactAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(signup)
admin.site.register(add_product)
admin.site.register(cart)
admin.site.register(Order)