from django.shortcuts import render, get_object_or_404,reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from scndapp.models import Contact,Category,signup,add_product,cart,Order
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate, logout
from django.contrib.auth.decorators import login_required
from scndapp.forms import add_product_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage

# Create your views here.
def home(rqst):
    if "user_id" in rqst.COOKIES:
        uid = rqst.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(rqst,usr)
        if usr.is_superuser:
            return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/customer")
        
    recent = Contact.objects.all().order_by("-id")[:5]
    cat = Category.objects.all().order_by("cname")

    return render(rqst,"home.html",{"messages":recent,"category":cat})

def about(rqst):
    cat = Category.objects.all().order_by("cname")
    return render(rqst,"about.html",{"category":cat})

def contact(rqst):
    cat = Category.objects.all().order_by("cname")
    alldata = Contact.objects.all().order_by("-id")
    if rqst.method=="POST":
        nm = rqst.POST["name"]
        con = rqst.POST["contact"]
        sub = rqst.POST["subject"]
        msg = rqst.POST["message"]
        
        data = Contact(name=nm,contact=con,subject=sub,message=msg)
        data.save()
        result = "Dear {} Thanks for feedback".format(nm)
        return render(rqst,"contact.html",{"status":result,"messages":alldata,"category":cat})
        # return HttpResponse("<h1 style='color:red'>Dear {}, your data saved successfully!!!</h1>".format(nm))
    return render(rqst,"contact.html",{"messages":alldata,"category":cat})

def register(rqst):
    if "user_id" in rqst.COOKIES:
        uid = rqst.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(rqst,usr)
        if usr.is_superuser:
            return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/customer")

    if rqst.method=="POST":
        fname = rqst.POST["first"]
        lname = rqst.POST["last"]
        uname = rqst.POST["username"]
        pswrd = rqst.POST["password"]
        em = rqst.POST["email"]
        con = rqst.POST["contact"]
        utype = rqst.POST["usertype"]

        usr = User.objects.create_user(uname,em,pswrd)
        usr.first_name = fname
        usr.last_name = lname
        if utype == "seller":
            usr.is_staff = True
        usr.save()

        reg = signup(contact=con, user=usr)
        reg.save()
        return render(rqst,"register.html",{"status":"Mr/Miss. {} your Account created successfully".format(fname)})
    return render(rqst,"register.html")

def check(rqst):
    if rqst.method == "GET":
        un = rqst.GET["user"]
        check = User.objects.filter(username=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not exists")

def signin(rqst):
    if rqst.method == "POST":
        un = rqst.POST["username"]
        pw = rqst.POST["password"]

        user = authenticate(username=un,password=pw)
        if user:
            login(rqst,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                res = HttpResponseRedirect("/customer")
                if "remember" in rqst.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res
        else:
            return render(rqst,"home.html",{"status":"Invalid Username or password"})

    return HttpResponse("Called")

@login_required
def customer(rqst):
    context = {}
    check = signup.objects.filter(user__id=rqst.user.id)
    if len(check) > 0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data
    return render(rqst,"customer.html",context)

@login_required
def seller(rqst):
    return render(rqst,"seller.html")

@login_required
def signout(rqst):
    logout(rqst)
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res

def edit_profile(rqst):
    context = {}
    check = signup.objects.filter(user__id=rqst.user.id)
    if len(check)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data
    if rqst.method == "POST":
        # print(rqst.FILES)
        fn = rqst.POST["fname"]
        ln = rqst.POST["lname"]
        em = rqst.POST["email"]
        con = rqst.POST["contact"]
        ag = rqst.POST["age"]
        ct = rqst.POST["city"]
        gen = rqst.POST["gender"]
        occ = rqst.POST["occupation"]
        abt = rqst.POST["about"]

        usr = User.objects.get(id=rqst.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()

        data.contact = con
        data.age = ag
        data.city = ct
        data.gender = gen
        data.occupation = occ
        data.about = abt
        data.save()

        if "image" in rqst.FILES:
            img = rqst.FILES["image"]
            data.pic = img
            data.save()

        context["status"] = "Changes saved successfully"
        
    return render(rqst,"edit.html",context)

def change_password(rqst):
    context = {}
    chck = signup.objects.filter(user__id=rqst.user.id)
    if len(chck)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data

    if rqst.method == "POST":
        cur_pas = rqst.POST["cpswrd"]
        new_pas = rqst.POST["npswrd"]

        user = User.objects.get(id=rqst.user.id)
        un = user.username
        pwd = new_pas
        check = user.check_password(cur_pas)
        if check == True:
            user.set_password(new_pas)
            user.save()
            context["msg"] = "Password changed successfully!!!"
            context["color"] = "alert-success"
            user = User.objects.get(username=un)
            login(rqst,user)
        else:
            context["msg"] = "Incorrect Current Password!!!"
            context["color"] = "alert-danger"

    return render(rqst,"change_password.html",context)

def addproduct(rqst):
    context = {}
    chck = signup.objects.filter(user__id=rqst.user.id)
    if len(chck)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data
    form = add_product_form()
    if rqst.method == "POST":
        form = add_product_form(rqst.POST,rqst.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            login_user = User.objects.get(username = rqst.user.username)
            data.seller = login_user
            data.save()
            context["status"] = "{} Added successfully!!!".format(data.product_name)

    context["form"] = form
    
    return render(rqst,"add_product.html",context)

def myproducts(rqst):
    context = {}
    chck = signup.objects.filter(user__id=rqst.user.id)
    if len(chck)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data
    all = add_product.objects.filter(seller__id = rqst.user.id).order_by("-id")
    context["products"] = all
    return render(rqst,"myproducts.html",context)

def singleproduct(rqst):
    context = {}
    id = rqst.GET["pid"]
    obj = add_product.objects.get(id=id)
    context["product"] = obj
    return render(rqst,"singleproduct.html",context)

def updateproduct(rqst):
    context = {}
    cat = Category.objects.all().order_by("cname")
    context["category"] = cat

    pid = rqst.GET["pid"]
    product = add_product.objects.get(id=pid)
    context["product"] = product

    if rqst.method == "POST":
        pn = rqst.POST["pname"]
        pc_id = rqst.POST["pcategory"]
        pp = rqst.POST["pprice"]
        sp = rqst.POST["sprice"]
        pd= rqst.POST["pdes"]

        cat_obj = Category.objects.get(id=pc_id)

        product.product_name = pn
        product.product_category = cat_obj
        product.product_price = pp
        product.sale_price = sp
        product.description = pd

        if "pimg" in rqst.FILES:
            img = rqst.FILES["pimg"]
            product.product_image = img
        product.save()
        context["status"] = "Changes Saved Successfully!!!"
        context["id"] = pid

    return render(rqst,"update_product.html",context)

def deleteproduct(rqst):
    context = {}
    if "pid" in rqst.GET:
        pid = rqst.GET["pid"]
        product = get_object_or_404(add_product,id=pid)
        context["product"] = product

        if "action" in rqst.GET:
            product.delete()
            context["status"] = "{} Deleted Successfully!!!".format(product.product_name)
    return render(rqst,"deleteproduct.html",context)

def allproducts(rqst):
    context = {}
    cat = Category.objects.all().order_by("cname")
    context["category"] = cat
    all_products = add_product.objects.all().order_by("product_name")
    context["products"] = all_products

    if "qry" in rqst.GET:
        q = rqst.GET["qry"]
        # p = rqst.GET["price"]
        
        prd = add_product.objects.filter(Q(product_name__icontains=q)|Q(product_category__cname__contains=q))
        # prd = add_product.objects.filter(Q(product_name__icontains=q)& Q(sale_price__lt=p))
        # prd = add_product.objects.filter(product_name__contains=q)
        context["products"] = prd
        context["abc"] = "search"

    if "cat" in rqst.GET:
        cid = rqst.GET["cat"]
        prd = add_product.objects.filter(product_category__id=cid)
        context["products"] = prd
        context["abc"] = "search"

    return render(rqst,"allproducts.html",context)

def sendemail(rqst):
    context = {}
    chck = signup.objects.filter(user__id=rqst.user.id)
    if len(chck)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data
    if rqst.method=="POST":
        rec = rqst.POST["to"].split(",")
        sub = rqst.POST["sub"]
        msg = rqst.POST["msg"]

        try:
            em = EmailMessage(sub,msg,to=rec)
            em.send()
            context["message"] = "Email Sent Successfully!!!"
            context["class"] = "alert-success"
        except:
            context["message"] = "Could'nt Sent, Please check internet connection / Email Address!!!"
            context["class"] = "alert-danger"
    return render(rqst,"email.html",context)

def forgot(rqst):
    context={}
    if rqst.method=="POST":
        un = rqst.POST["uname"]
        pwd = rqst.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(rqst,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/customer")
        context["status"] = "Password Reset Successfully!!"
    return render(rqst,"forgot.html",context)

import random
def reset(rqst):
    un = rqst.GET["username"]
    try:
        user = get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msg = "Dear {} \n{}Here is Your One Time Password(OTP) \nDo not share it with others \nThanks&Regards \nMy Website".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msg,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})

    except:
        return JsonResponse({"status":"not found"})

def add_to_cart(rqst):
    context = {}
    items = cart.objects.filter(user__id=rqst.user.id,status=False)
    context["items"] = items
    if rqst.user.is_authenticated:
        if rqst.method == "POST":
            pid = rqst.POST["pid"]
            qty = rqst.POST["qty"]
            is_exist = cart.objects.filter(product__id=pid,user__id=rqst.user.id,status=False)
            if len(is_exist) > 0:
                context["msg"] = "Item already exists in your cart!!"
                context["class"] = "alert alert-warning"
            else:
                product = get_object_or_404(add_product,id=pid)
                usr = get_object_or_404(User,id=rqst.user.id)
                c = cart(user=usr,product=product,quantity=qty)
                c.save()
                context["msg"] = "{} added succesfully in your cart!!!".format(product.product_name)
                context["class"] = "alert alert-success"
    else:
        context["status"] = "Please Login First to View your Cart!!!"
    return render(rqst,"cart.html",context)

def get_cart_data(rqst):
    items = cart.objects.filter(user__id=rqst.user.id, status=False)
    sale,total,quantity = 0,0,0
    for i in items:
        sale += float(i.product.sale_price)*i.quantity
        total += float(i.product.product_price)*i.quantity
        quantity += float(i.quantity)

    res = {
        "total":total,"offer":sale,"quan":quantity
    }
    return JsonResponse(res)

def change_quantity(rqst):
    if "quantity" in rqst.GET:
        cid = rqst.GET["cid"]
        qty = rqst.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=cid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)

    if "delete_cart" in rqst.GET:
        id = rqst.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

def process_payment(request):
    items = cart.objects.filter(user_id__id = request.user.id,status=False)
    products = ""
    p_ids = ""
    amount = 0
    invoice = "INV1001-"
    cart_ids = ""
    for j in items:
        products += str(j.product.product_name)+"\n"
        p_ids += str(j.product.id)+","
        amount += float(j.product.sale_price)
        invoice += str(j.id)
        cart_ids += str(j.id)+","
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amount),
        'item_name': products,
        'invoice': invoice,
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),
    }
    usr = User.objects.get(username=request.user.username)
    ord = Order(cust_id=usr,cart_id=cart_ids,product_ids=p_ids,invoice_id=invoice)
    ord.save()
    ord.invoice_id = str(ord.id)+invoice
    ord.save()
    request.session["order_id"] = ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})

def payment_done(rqst):
    if "order_id" in rqst.session:
        order_id = rqst.session["order_id"]
        order_obj = get_object_or_404(Order,id=order_id)
        order_obj.status = True
        order_obj.save()
        
        for i in order_obj.cart_id.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status = True
            cart_object.save()

    return render(rqst,"payment_success.html")

def payment_cancelled(rqst):
    return render(rqst,"payment_fail.html")

def order_history(rqst):
    context = {}
    chck = signup.objects.filter(user__id=rqst.user.id)
    if len(chck)>0:
        data = signup.objects.get(user__id=rqst.user.id)
        context["data"] = data

    all_orders=[]
    orders = Order.objects.filter(cust_id__id=rqst.user.id).order_by("-id")
    for order in orders:
        products = []
        for id in order.product_ids.split(",")[:-1]:
            pro = get_object_or_404(add_product, id=id)
            products.append(pro)
        ord = {
            "order_id":order.id,
            "products":products,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on
        }
        all_orders.append(ord)
    context["order_history"] = all_orders
    return render(rqst,"order_history.html",context)

def my_customers(rqst):
    context = {}
    products = cart.objects.filter(product__seller__id=rqst.user.id,status=True)
    cust = []
    ids = []
    context["times"] = len(products) 
    for i in products:
        us ={
            "username":i.user.username,
            "first_name":i.user.first_name,
            "last_name":i.user.last_name,
            "email":i.user.email,
            "join":i.user.date_joined,
        }
        check = signup.objects.filter(user__id=i.user.id)
        if len(check)>0:
            prf=get_object_or_404(signup,user__id=i.user.id)
            us["profile_pic"] = prf.pic
            us["contact"] = prf.contact
        ids.append(i.user.id)
        count = ids.count(i.user.id)

        if count<2:
            cust.append(us)
            count+=1

    context["customers"] = cust

    return render(rqst,"my_customers.html",context)