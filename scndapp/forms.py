from django import forms
from scndapp.models import add_product

class add_product_form(forms.ModelForm):
    class Meta:
        model = add_product
        # fields = "__all__"
        # fields = ["product_name","product_category","product_price","sale_price","product_image","description"]
        exclude = ["seller"]