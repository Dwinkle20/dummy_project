from django.db import models
from django.contrib.auth.models import User

import datetime
# cd = datetime.datetime.now().date
# Create your models here.

class Student(models.Model):
    choice = (
            ("M","Male"),("F","Female")
        )
    name = models.CharField(max_length=250)              # max length is manadatory
    email = models.EmailField(max_length=200)            # max length is not manadatory
    rno = models.IntegerField(unique=True)
    # dob = models.DateField(blank=True,default=None)
    fee = models.FloatField()  
    gender = models.CharField(max_length=150,choices=choice)
    address = models.TextField(blank=True)        # TextField can have unlimited length
                            # blank true for making it optional
    is_registered =  models.BooleanField()

    def __str__(self):                              # return only strings
        return self.nam     #it will return that thing tht we want, instead of object1,object2
        # return str(self.rno)
        
    class Meta:
        verbose_name_plural = "Student"

class Contact(models.Model):
    name = models.CharField(max_length=250)
    contact = models.IntegerField(blank=True,unique=True)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):                             
        return self.name

    class Meta:
        verbose_name_plural = "Contact"

class Category(models.Model):
    cname = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to="media/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cname

class signup(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    pic = models.ImageField(upload_to = "profiles/%Y/%m/%d",null=True,blank=True)
    age = models.CharField(max_length=250,null=True,blank=True)
    city = models.CharField(max_length=250,null=True,blank=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=250,default="Male",blank=True)
    occupation = models.CharField(max_length=250,null=True,blank=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username

class add_product(models.Model):
    seller = models.ForeignKey(User,on_delete = models.CASCADE)
    product_name = models.CharField(max_length=250)
    product_category = models.ForeignKey(Category,on_delete = models.CASCADE)
    product_price = models.FloatField()
    sale_price = models.CharField(max_length=250)
    product_image = models.ImageField(upload_to="products/%Y/%m/%d")
    description = models.TextField()

    def __str__(self):
        return self.product_name

    class Meta:
        verbose_name_plural = "add_product"

class cart(models.Model):
    user = models.ForeignKey(User,on_delete = models.CASCADE)
    product = models.ForeignKey(add_product,on_delete = models.CASCADE)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = "cart"

class Order(models.Model):
    cust_id = models.ForeignKey(User,on_delete=models.CASCADE)
    cart_id = models.CharField(max_length=250)
    product_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cust_id.username

    class Meta:
        verbose_name_plural = "Order"