"""scndproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from scndapp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name="home"),
    path('about/',views.about,name="about"),
    path('contact/',views.contact,name="contact"),
    path('signup/',views.register,name="signup"),
    path('check/',views.check,name="check"),
    path('signin/',views.signin,name="signin"),
    path('customer/',views.customer,name="customer"),
    path('seller/',views.seller,name="seller"),
    path('signout/',views.signout,name="signout"),
    path('edit_profile/',views.edit_profile,name="edit_profile"),
    path('change_password/',views.change_password,name="change_password"),
    path('addproduct/',views.addproduct,name="addproduct"),
    path('myproducts/',views.myproducts,name="myproducts"),
    path('singleproduct/',views.singleproduct,name="singleproduct"),
    path('updateproduct/',views.updateproduct,name="updateproduct"),
    path('deleteproduct/',views.deleteproduct,name="deleteproduct"),
    path('allproducts/',views.allproducts,name="allproducts"),
    path('sendemail/',views.sendemail,name="sendemail"),
    path('forgot/',views.forgot,name="forgot"),
    path('reset/',views.reset,name="reset"),
    path('add_to_cart/',views.add_to_cart,name="add_to_cart"),
    path('get_cart_data/',views.get_cart_data,name="get_cart_data"),
    path('change_quantity/',views.change_quantity,name="change_quantity"),
    
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('process_payment/',views.process_payment,name="process_payment"),
    path('payment_done/',views.payment_done,name="payment_done"),
    path('payment_cancelled/',views.payment_cancelled,name="payment_cancelled"),
    path('order_history/',views.order_history,name="order_history"),
    path('my_customers/',views.my_customers,name="my_customers"),
]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
